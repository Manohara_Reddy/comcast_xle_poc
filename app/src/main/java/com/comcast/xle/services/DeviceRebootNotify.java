package com.comcast.xle.services;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

public class DeviceRebootNotify extends BroadcastReceiver {


    @Override
    public void onReceive(Context context, Intent intent) {
        if ("android.intent.action.BOOT_COMPLETED".equals(intent.getAction())) {
            // Start background service here for sending the data
            Log.d("Mansi", "onReceive: Device Rebot");
        }
    }
}
