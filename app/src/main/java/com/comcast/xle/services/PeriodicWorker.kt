package com.comcast.xle.services

import android.content.Context
import android.os.Handler
import android.os.Looper
import android.util.Log
import android.widget.Toast
import androidx.work.Worker
import androidx.work.WorkerParameters
import com.comcast.xle.AllDataHelper
import com.comcast.xle.EmailHelper

class PeriodicWorker(context: Context, params: WorkerParameters) : Worker(context, params) {



    override fun doWork(): Result {
        val appContext = applicationContext
       Log.d("Manohar", "doWork executing")
        val handler = Handler(Looper.getMainLooper())
        handler.postDelayed({ // Run your task here
            Toast.makeText(appContext, "XLE data fetched in background", Toast.LENGTH_SHORT).show()
            val allDataHelper = AllDataHelper()
            val allValues = allDataHelper.fetchAllData(appContext)
            Log.d("Manohar", allValues.toString())
            EmailHelper().sendEmail(allValues)
        }, 1000)

        return Result.success()
    }


}