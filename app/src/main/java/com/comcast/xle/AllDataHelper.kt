package com.comcast.xle

import android.content.Context
import android.os.Build
import android.telephony.*
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import java.util.*

class AllDataHelper {

    companion object {
        var LATITUDE = ""
        var LONGITUDE = ""
        var IP_ADDRESS = ""
    }
    private var gpsTracker: GpsTracker? = null

     fun getLocation(context: Context) {
        gpsTracker = GpsTracker(context)
        if (gpsTracker!!.canGetLocation()) {
            val latitude: Double = gpsTracker!!.getLatitude()
            val longitude: Double = gpsTracker!!.getLongitude()
            LATITUDE = latitude.toString()
            LONGITUDE = longitude.toString()

            Log.d("Manohar", "lat: " + latitude)
            Log.d("Manohar", "lng: " + longitude)
//            Toast.makeText(
//                this, "lat: " + latitude.toString() + "  " +
//                        "lng: " + latitude.toString(), Toast.LENGTH_SHORT
//            ).show()
        } else {
            gpsTracker!!.showSettingsAlert()
        }
    }


    fun fetchAllData(context: Context) : ArrayList<String>{
        getLocation(context)
        val ipAddress = IPAddressHelper.getIPAddress(true)
        IP_ADDRESS = ipAddress + ""
        var cellInfoList: List<CellSignalStrength>?
        var cellInfoListNew: List<CellInfo>?
        val data = ArrayList<String>()
        //        MCC, MNC, PCI, RSSNR, RSRP, RSRQ
        val tm = context.getSystemService(AppCompatActivity.TELEPHONY_SERVICE) as TelephonyManager
        val countryCodeValue = context.resources.configuration.mcc
        val mobileNetworkCodes = context.resources.configuration.mnc
       // checkPermission(Manifest.permission.ACCESS_FINE_LOCATION, 0)

        cellInfoListNew = tm.allCellInfo
        var rsrp = 0;
        var rsrq = 0;
        var snr = 0;
        var cellPci = 0;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
                cellInfoList = tm.signalStrength!!.cellSignalStrengths

                for (cellInfo in cellInfoList) {
                    if (cellInfo is CellSignalStrengthLte) {
                        rsrp = cellInfo.rsrp
                        rsrq = cellInfo.rsrq
                        snr = cellInfo.rssnr
                    }
                }
            }
        }
        for (cellInfo in cellInfoListNew) {
            if (cellInfo is CellInfoLte) {
                cellPci = cellInfo.cellIdentity.pci
            }
        }

        val mPhoneNumber = context.getSystemService(AppCompatActivity.TELEPHONY_SERVICE) as TelephonyManager
        var phone= mPhoneNumber.line1Number

        data.add("Phone Number: $phone")
        data.add("LTE MCC: $countryCodeValue")
        data.add("LTE MNC: $mobileNetworkCodes")
        data.add("LTE PCI: $cellPci")
        data.add("LTE RSRP: $rsrp")
        data.add("LTE RSRQ: $rsrq")
        data.add("LTE SNR: $snr")
        data.add("Latitude: ${LATITUDE}")
        data.add("Longitude: ${LONGITUDE}")
        data.add("IP Address: $IP_ADDRESS")

        return data
    }
}