package com.comcast.xle.ui

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.comcast.xle.databinding.ActivityReadmeBinding


class ReadmeActivity : AppCompatActivity() {

    private lateinit var binding: ActivityReadmeBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityReadmeBinding.inflate(layoutInflater)
        setContentView(binding.root)
        setSupportActionBar(binding.toolbar)
        supportActionBar?.title = "INSTRUCTIONS"

        setAdapter()
    }

    private fun setAdapter() {
        val recyclerView = binding.recyclerview
        recyclerView.layoutManager = LinearLayoutManager(this)
        val data = ArrayList<String>()
        data.add("User need to check if below settings are enabled for getting correct info.")
        data.add("Enable location permission when asked for.")
        data.add("If Location permission was denied by user then he need to manually go to Setting >> Apps >> Enable location permission.")
        data.add("User need to provide permission to read phone state in order to get the use phone number.")
        data.add("If the permission is denied then he need to manually go to app settings and enable it.")
        data.add("If the user had denied the permissions 2 times then it will not prompt upon opening the app.")
        data.add("If there was an issue while giving permission when prompted, Go to settings >> apps >> and clear the data.")
        data.add("Clear app data and reopen the app and provide the required permissions.")
        data.add("IMPORTANT: Location should always be ON to be able to generate report and send it in background.")

        val adapter = ReadmeAdapter(data)
        recyclerView.adapter = adapter

    }
}