package com.comcast.xle.ui


import android.Manifest
import android.app.ActivityManager
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.net.ConnectivityManager
import android.net.TrafficStats
import android.net.wifi.WifiManager
import android.os.Build
import android.os.Bundle
import android.telephony.*
import android.text.format.Formatter.formatIpAddress
import android.util.Log
import android.widget.Button
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.work.PeriodicWorkRequest
import androidx.work.PeriodicWorkRequest.MIN_PERIODIC_INTERVAL_MILLIS
import androidx.work.WorkManager
import com.comcast.xle.*
import com.comcast.xle.AllDataHelper.Companion.IP_ADDRESS
import com.comcast.xle.AllDataHelper.Companion.LATITUDE
import com.comcast.xle.AllDataHelper.Companion.LONGITUDE
import com.comcast.xle.services.PeriodicWorker
import java.net.InetAddress
import java.net.NetworkInterface
import java.util.*
import java.util.concurrent.TimeUnit

class MainActivity : AppCompatActivity() {

    private val PERMISSION_ID = 101
    private val WORKER_TAG = "Worker_Tag"
    private lateinit var recyclerView: RecyclerView;
    private lateinit var adapter : KPICollectionDataAdapter;

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val btnFetch = findViewById<Button>(R.id.btn_fetch)
        val btnReadme = findViewById<Button>(R.id.btn_readme)
        recyclerView = findViewById<RecyclerView>(R.id.recyclerview)
        recyclerView.layoutManager = LinearLayoutManager(this)
        val data = ArrayList<String>()
        data.add("LTE MCC")
        data.add("LTE MNC")
        data.add("LTE PCI")
        data.add("LTE RSSNR")
        data.add("LTE RSRP")
        data.add("LTE RSRQ")
        data.add("Latitude")
        data.add("Longitude")
        data.add("IP Address")
        var linearLayoutManager = LinearLayoutManager(this);

        val dividerItemDecoration = DividerItemDecoration(
            recyclerView.context,
            linearLayoutManager.orientation
        )
        //recyclerView.addItemDecoration(dividerItemDecoration)
        //  recyclerView.addItemDecoration(DividerItemDecoration(this@MainActivity, LinearLayoutManager.VERTICAL))
        //   recyclerView.itemAnimator=DefaultItemAnimator();
        if (checkLocationPermissions()) {
            fetchData()
        } else {
            requestLocationPermission(this)
        }
       // Toast.makeText(this, "IP Address: " + IP_ADDRESS, Toast.LENGTH_SHORT).show()
        // val emailHelper = EmailHelper().sendEmail()
        btnFetch.setOnClickListener {
            if (checkLocationPermissions()) {
                if(adapter!=null) {
                    adapter.updateAllTheData(getAllData())
                    EmailHelper().sendEmail(getAllData())
                    Toast.makeText(this, "Updated data fetched...", Toast.LENGTH_SHORT).show()
                }
            } else {
                requestLocationPermission(this)
            }
        }

        btnReadme.setOnClickListener {
            val intent = Intent(this, ReadmeActivity::class.java)
            startActivity(intent)
        }
    }


    fun getAllData(): ArrayList<String> {
        getLocation()
        val ipAddress = IPAddressHelper.getIPAddress(true)
        IP_ADDRESS = ipAddress + ""
        var cellInfoList: List<CellSignalStrength>? = null
        var cellInfoListNew: List<CellInfo>? = null
        val data = ArrayList<String>()
        //        MCC, MNC, PCI, RSSNR, RSRP, RSRQ
        val tm = getSystemService(TELEPHONY_SERVICE) as TelephonyManager
        val countryCodeValue = resources.configuration.mcc
        val mobileNetworkCodes = resources.configuration.mnc

        cellInfoListNew = tm.allCellInfo

        var rsrp=0;var rsrq=0;var rssnr=0;var cellPci=0;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
                cellInfoList = tm.signalStrength!!.cellSignalStrengths

                for (cellInfo in cellInfoList) {
                    if (cellInfo is CellSignalStrengthLte) {
                        rsrp = cellInfo.rsrp
                        rsrq = cellInfo.rsrq
                        rssnr = cellInfo.rssnr
                    }
                }
            }
        }
        for (cellInfo in cellInfoListNew) {
            if (cellInfo is CellInfoLte) {
                cellPci = cellInfo.cellIdentity.pci
            }
        }
        val connectivityManager = this.getSystemService(CONNECTIVITY_SERVICE) as ConnectivityManager
        val nc = connectivityManager.getNetworkCapabilities(connectivityManager.activeNetwork)
        val downSpeed = nc!!.linkDownstreamBandwidthKbps
        val upSpeed = nc!!.linkUpstreamBandwidthKbps



        // Get running processes
        // Get running processes
        val manager = getSystemService(ACTIVITY_SERVICE) as ActivityManager
        val runningApps = manager.runningAppProcesses
        for (runningApp in runningApps) {
            val received = TrafficStats.getUidRxBytes(runningApp.uid)
            val sent = TrafficStats.getUidTxBytes(runningApp.uid)
            Log.d(
                "Mansi", java.lang.String.format(
                    Locale.getDefault(),
                    "uid: %1d - name: %s: Sent = %1d, Rcvd = %1d",
                    runningApp.uid,
                    runningApp.processName,
                    sent,
                    received
                )
            )
        }

        val mPhoneNumber = getSystemService(TELEPHONY_SERVICE) as TelephonyManager
        var phone= mPhoneNumber.line1Number

        data.add("Phone Number: $phone")
        data.add("LTE MCC: $countryCodeValue")
        data.add("LTE MNC: $mobileNetworkCodes")
        data.add("LTE PCI: $cellPci")
        data.add("LTE RSRP: $rsrp" +" dBm")
        data.add("LTE RSRQ: $rsrq" +" dBm")
        data.add("LTE RSSNR: $rssnr"+" dBm")
        data.add("Latitude: ${LATITUDE}")
        data.add("Longitude: ${LONGITUDE}")
        data.add("IP Address: $IP_ADDRESS")

        return data
    }



    private fun fetchData() {
        adapter = KPICollectionDataAdapter(getAllData())
        recyclerView.adapter = adapter
        runPeriodicWorker()
    }

    fun getLocation() {
        val allDataHelper = AllDataHelper()
        allDataHelper.getLocation(this)
    }

    private fun requestLocationPermission(context: Context) {
        try {
            if (ContextCompat.checkSelfPermission(
                    context,
                    Manifest.permission.ACCESS_FINE_LOCATION,

                ) != PackageManager.PERMISSION_GRANTED
                ||
                ContextCompat.checkSelfPermission(
                    context,
                    Manifest.permission.READ_PHONE_STATE,

                    ) != PackageManager.PERMISSION_GRANTED ||
                ContextCompat.checkSelfPermission(
                    context,
                    Manifest.permission.READ_PHONE_NUMBERS,

                    ) != PackageManager.PERMISSION_GRANTED

            ) {
                ActivityCompat.requestPermissions(
                    this, arrayOf(Manifest.permission.ACCESS_FINE_LOCATION,
                        Manifest.permission.READ_PHONE_STATE,
                        Manifest.permission.READ_PHONE_NUMBERS), PERMISSION_ID
                )
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }

    }


    private fun checkLocationPermissions(): Boolean {
        if (
            ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) ==
            PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission
                (
                this,
                Manifest.permission.ACCESS_FINE_LOCATION
            ) == PackageManager.PERMISSION_GRANTED
            &&  ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE) ==
            PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission
                (
                this,
                Manifest.permission.READ_PHONE_STATE
            ) == PackageManager.PERMISSION_GRANTED
            &&  ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_NUMBERS) ==
            PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission
                (
                this,
                Manifest.permission.READ_PHONE_NUMBERS
            ) == PackageManager.PERMISSION_GRANTED
        ) {
            return true
        }
        return false
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == PERMISSION_ID) {
            if ((grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
                fetchData()
            } else {
                Toast.makeText(this, "Go to settings >> Apps >> Enable location permission to start using app.",
                    Toast.LENGTH_SHORT).show()
            }
        }
    }

    fun getMobileIPAddress(): String? {
        try {
            val interfaces: List<NetworkInterface> =
                Collections.list(NetworkInterface.getNetworkInterfaces())
            for (intf in interfaces) {
                val addrs: List<InetAddress> = Collections.list(intf.getInetAddresses())
                for (addr in addrs) {
                    if (!addr.isLoopbackAddress()) {
                        return addr.getHostAddress()
                    }
                }
            }
        } catch (ex: Exception) {
        } // for now eat exceptions
        return ""
    }

    fun getWifiIPAddress(): String? {
        val wifiMgr = getSystemService(WIFI_SERVICE) as WifiManager
        val wifiInfo = wifiMgr.connectionInfo
        val ip = wifiInfo.ipAddress
        return formatIpAddress(ip)
    }

    private fun runPeriodicWorker() {
        val periodicWorkRequest = PeriodicWorkRequest.Builder(
            PeriodicWorker::class.java,
            MIN_PERIODIC_INTERVAL_MILLIS,
            TimeUnit.MILLISECONDS
        )
            .setInitialDelay(10000, TimeUnit.MILLISECONDS)
            .addTag(WORKER_TAG).build()
        val workManager = WorkManager.getInstance(applicationContext)
        workManager.cancelAllWorkByTag(WORKER_TAG)
        workManager.enqueue(periodicWorkRequest)
    }
}