package com.comcast.xle.ui

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.comcast.xle.R

class KPICollectionDataAdapter (private var mList: List<String>): RecyclerView.Adapter<KPICollectionDataAdapter.ViewHolder>() {

     private lateinit var context:Context;
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        context=parent.context;
        val view= LayoutInflater.from(parent.context).inflate(R.layout.kpi_collection_data_layout,parent,false)
    return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
       /* if (position % 2 == 0) {
            holder.lyMainLayout.setBackgroundColor(ContextCompat.getColor(context,R.color.black))
        } else {
            holder.lyMainLayout.setBackgroundColor(ContextCompat.getColor(context,R.color.black_overlay))

        }*/
        var array=mList[position].split(":")
        holder.txtTitle.text=array[0]
        holder.txtInfo.text=array[1]
    }

    override fun getItemCount(): Int {
        return mList.size
    }
    fun updateAllTheData(updateArrayList: List<String>) {
        mList=updateArrayList
        notifyDataSetChanged()

    }
    class ViewHolder(ItemView: View) : RecyclerView.ViewHolder(ItemView) {
    val txtTitle:TextView=ItemView.findViewById(R.id.txt_data_title);
    val txtInfo:TextView=ItemView.findViewById(R.id.txt_data_info);
        val lyMainLayout: ConstraintLayout =ItemView.findViewById(R.id.ly_main_layout)
    }
}